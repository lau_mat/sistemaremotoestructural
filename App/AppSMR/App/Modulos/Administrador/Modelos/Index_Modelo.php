<?php
	
	/**
	 * Clase: Index_Modelo
	 */
	class Index_Modelo extends Modelo {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Metodo: Ejemplo
		 */
		public function ConsultaSQL() {
		}
        
        public function ConsultarAsistenteTaller(){
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_talleres');
            $Consulta->Columnas("tbl_talleres.Nombre,COUNT(*) AS Asistentes");
            $Consulta->InnerJoin('tbl_talleres_asistentes', 'tbl_talleres.IdTaller', 'tbl_talleres_asistentes.IdTaller');
            $Consulta->Agrupar('tbl_talleres.Nombre');
            $Consulta->Ordenar('tbl_talleres.Nombre');
            return $Consulta->Ejecutar(false,true);
        }
	}