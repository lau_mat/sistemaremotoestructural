<?php
class Canales_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarCanales()
     *
     * Devuelve los canales.
     * @return mixed
     */
    public function ConsultarCanales($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_canales', false, false, APP));
        $SQL = "SELECT $Campos FROM tbl_canales ";
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' where '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * GuardaCanal($Datos = false)
     *
     * Guarda un Canal
     * @param bool $Dato
     * @return mixed
     */
    public function GuardaCanal($Datos = false){
        if ($Datos == true AND is_array($Datos) == true) {
            try {
                $this->Conexion->insert('tbl_canales', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * EditarDatos($Datos = false, $IdCanal = false)
     *
     * Editar datos del Canal
     * @param bool $Datos
     * @param bool $IdCanal
     */
    public function EditarDatos($Datos = false, $IdCanal = false){
        if($Datos == true AND is_array($Datos) == true AND $IdCanal == true AND $IdCanal != ''){
            try{
                return $this->Conexion->update('tbl_canales', $Datos, array('IdCanal'=>$IdCanal));
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }
}