<?php
class Sensores_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarSensores()
     *
     * Devuelve los Sensores Activos.
     * @return mixed
     */
    public function ConsultarSensores($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_sensores', false, false, APP));
        $SQL = "SELECT $Campos FROM tbl_sensores ";
        $SQL.=' where status = "ACTIVO" ';
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' and '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * GuardaSensor($Datos = false)
     *
     * Guarda un Sensor
     * @param bool $Dato
     * @return mixed
     */
    public function GuardaSensor($Datos = false){
        if ($Datos == true AND is_array($Datos) == true) {
            try {
                $this->Conexion->insert('tbl_sensores', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * EditarDatos($Datos = false, $IdSensor = false)
     *
     * Editar datos del Sensor
     * @param bool $Datos
     * @param bool $IdSensor
     */
    public function EditarDatos($Datos = false, $IdSensor = false){
        if($Datos == true AND is_array($Datos) == true AND $IdSensor == true AND $IdSensor != ''){
            try{
                return $this->Conexion->update('tbl_sensores', $Datos, array('IdSensor'=>$IdSensor));
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * @param bool $IdSensor
     *
     * Metodo Publico Eliminar Sensor
     * Cambia el Status del Sensor asociado al id
     * por "ELIMINADO"
     */
    public function Eliminar($IdSensor = false){
        if($IdSensor == true AND $IdSensor != ''){
            try{
                $this->Conexion->update('tbl_sensores',array('Status'=>"ELIMINADO"), array('IdSensor'=>$IdSensor));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }
}