<?php
class Supervisores_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarSupervisores()
     *
     * Devuelve los Supervisores Activos.
     * @return mixed
     */
    public function ConsultarSupervisores($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_informacion_usuarios', array('IdUsuario', 'Status'), false, APP));
        $Campos.= ', '.implode(',', self::ListarColumnas('tbl_sistema_usuarios', array('IdPerfil', 'Password'),false, APP));
        $SQL = "SELECT $Campos FROM tbl_informacion_usuarios ";
        $SQL.=" INNER JOIN tbl_sistema_usuarios ON  tbl_informacion_usuarios.IdUsuario = tbl_sistema_usuarios.IdUsuario";
        $SQL.=' WHERE tbl_sistema_usuarios.status != "ELIMINADO" AND tbl_sistema_usuarios.IdPerfil != 1 ';
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' AND '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param bool $IdUsuario
     * bool $Status
     *
     * Metodo Publico Activa o Desactiva Usuario
     * Cambia el Status del Usuario asociado al id
     * por "ACTIVO" u "DESACTIVADO"
     */
    public function ActivaDesactiva($IdUsuario = false, $Status = false){
        if($IdUsuario == true AND $IdUsuario != '' AND $Status == true AND $Status != ''){
            try{
                $this->Conexion->update('tbl_sistema_usuarios',array('Status'=>$Status), array('IdUsuario'=>$IdUsuario));
                $this->Conexion->update('tbl_informacion_usuarios',array('Status'=>$Status), array('IdUsuario'=>$IdUsuario));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    /**
     * @param bool $IdUsuario = false, $DatosUsuario = false, $Datos = false
     *
     * Metodo Publico EditarUsuario
     * Cambia Usuario Correo y Password del Usuario asociado al id
     */
    public function EditarUsuario($IdUsuario = false,  $DatosUsuario = false, $Datos = false){
        if($IdUsuario == true AND $IdUsuario != '' AND $Datos == true AND $Datos != '' AND $DatosUsuario == true AND $DatosUsuario != ''){
            try{
                $this->Conexion->update('tbl_sistema_usuarios',$DatosUsuario, array('IdUsuario'=>$IdUsuario));
                $this->Conexion->update('tbl_informacion_usuarios',$Datos, array('IdUsuario'=>$IdUsuario));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    /**
     * @param bool $IdUsuario
     *
     * Metodo Publico Eliminar Usuario
     * Cambia el Status del Usuario asociado al id
     * por "ELIMINADO"
     */
    public function Eliminar($IdUsuario = false){
        if($IdUsuario == true AND $IdUsuario != ''){
            try{
                $this->Conexion->update('tbl_sistema_usuarios',array('Status'=>"ELIMINADO"), array('IdUsuario'=>$IdUsuario));
                $this->Conexion->update('tbl_informacion_usuarios',array('Status'=>"ELIMINADO"), array('IdUsuario'=>$IdUsuario));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }
}