<?php
class Sensores extends Controlador{
    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Telefono = $this->Informacion['Informacion']['Telefono'];
        $Correo = $this->Informacion['Informacion']['Correo'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        $Plantilla->Parametro('Telefono', $Telefono);
        $Plantilla->Parametro('Correo', $Correo);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensores', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Telefono, $Correo, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista los Sensores
     */
    public function frmListado(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultarSensores();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensores', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar Sensor nodo.
     * @throws NeuralException
     */
    public function frmAgregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']){
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Mac', '* Campo Requerido');
            $Validacion->Requerido('Nombre', '* Campo Requerido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarSensor'));
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensores', 'Agregar', 'frmAgregar.html')));
            unset($Validacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * Agregar()
     *
     * Funcion de agregar Sensores
     * @throws NeuralException
     */
    public function Agregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $this->Modelo->GuardaSensor($DatosPost);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensores', 'Agregar', 'Exito.html')));
                unset($DatosPost, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * frmEditar()
     *
     * Formulario para editar Sensores.
     * @throws NeuralException
     */
    public function frmEditar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdSensor']) == true AND $_POST['IdSensor'] != '') {
                $IdSensor = NeuralCriptografia::DeCodificar($_POST['IdSensor'], APP);
                $Consulta = $this->Modelo->ConsultarSensores(array('tbl_sensores.IdSensor' => $IdSensor));
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('Nombre', '* Campo Requerido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarSensor'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensores', 'Editar', 'frmEditar.html')));
                unset($IdSensor, $Consulta, $Validacion, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Funcion de editar Sensores
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $IdCanal = NeuralCriptografia::DeCodificar($_POST['IdSensor'], APP);
                unset($_POST['IdSensor'],$_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $this->Modelo->EditarDatos($DatosPost, $IdCanal);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensores', 'Agregar', 'Exito.html')));
                unset($IdCanal, $DatosPost, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * EliminarRegistro()
     *
     * Recibe el arreglo post con el id del Sensor
     * y cambia su Status a "ELIMINADO"
     */
    public function EliminarRegistro(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND $_POST['IdSensor'] != "") {
                $IdSensor = NeuralCriptografia::DeCodificar($_POST['IdSensor'], APP);
                $this->Modelo->Eliminar($IdSensor);
            }
        }
    }
}