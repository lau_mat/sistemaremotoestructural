<?php
class Supervisores extends Controlador{
    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     */
    public function Index(){
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Telefono = $this->Informacion['Informacion']['Telefono'];
        $Correo = $this->Informacion['Informacion']['Correo'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        $Plantilla->Parametro('Telefono', $Telefono);
        $Plantilla->Parametro('Correo', $Correo);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisores', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Telefono, $Correo, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista los Supervisores
     */
    public function frmListado(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultarSupervisores();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisores', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * frmEditar()
     *
     * Formulario para editar Supervisor.
     * @throws NeuralException
     */
    public function frmEditar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdUsuario']) == true AND $_POST['IdUsuario'] != '') {
                $IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                $Consulta = $this->Modelo->ConsultarSupervisores(array('tbl_sistema_usuarios.IdUsuario' => $IdUsuario));
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('Usuario', '* Campo Requerido');
                $Validacion->Requerido('Correo', '* Campo Requerido');
                $Validacion->Requerido('RepiteCorreo', '* Campo Requerido');
                $Validacion->CampoIgual('RepiteCorreo','Correo');
                $Validacion->Email('Correo', '* Email formato invalido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarSupervisor'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisores', 'Editar', 'frmEditar.html')));
                unset($IdUsuario, $Consulta, $Validacion, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Funcion de editar Supervisores
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                unset($_POST['RepiteCorreo'], $_POST['Key'], $_POST['IdUsuario']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $Password=$DatosPost['Password'];
                if($Password != null)
                    $DatosLogueo = array('Usuario' => $DatosPost['Usuario'], 'Password' =>  hash('sha256',$Password));
                else
                    $DatosLogueo = array('Usuario' => $DatosPost['Usuario']);
                $this->Modelo->EditarUsuario($IdUsuario, $DatosLogueo, array('Correo' => $DatosPost['Correo']));
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisores', 'Editar', 'Exito.html')));
                unset($IdUsuario, $DatosPost, $DatosLogueo, $Password, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * ActivaDesactivaUsuario()
     *
     * Recibe el arreglo post con el id del Supervisor y su Status actual
     * y cambia su Status"
     */
    public function ActivaDesactivaUsuario(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND $_POST['IdUsuario'] != "" AND $_POST['Status'] != "") {
                $IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                $Status = $_POST['Status'];
                if ($Status=='ACTIVO'){
                    $Status='DESACTIVADO';
                }else{
                    $Status='ACTIVO';
                }
                $this->Modelo->ActivaDesactiva($IdUsuario, $Status);
                unset($IdUsuario, $Status, $_POST);
            }
        }
    }

    /**
     * Metodo Publico
     * EliminarRegistro()
     *
     * Recibe el arreglo post con el id del Supervisor
     * y cambia su Status a "ELIMINADO"
     */
    public function EliminarRegistro(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND $_POST['IdUsuario'] != "") {
                $IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                $this->Modelo->Eliminar($IdUsuario);
            }
        }
    }
}