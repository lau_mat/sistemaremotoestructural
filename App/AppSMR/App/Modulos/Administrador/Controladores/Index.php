<?php

	class Index extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct() {
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla Principal del sistema
		 *
		 */
		public function Index() {
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Telefono = $this->Informacion['Informacion']['Telefono'];
			$Correo = $this->Informacion['Informacion']['Correo'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('Telefono', $Telefono);
			$Plantilla->Parametro('Correo', $Correo);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Principal', 'Index.html')));
			unset($MenuSeleccion, $TipoUsuario, $Usuario, $Telefono, $Correo, $Plantilla);
			exit();
		}

	}