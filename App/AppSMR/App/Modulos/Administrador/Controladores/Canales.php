<?php
class Canales extends Controlador{
    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Telefono = $this->Informacion['Informacion']['Telefono'];
        $Correo = $this->Informacion['Informacion']['Correo'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        $Plantilla->Parametro('Telefono', $Telefono);
        $Plantilla->Parametro('Correo', $Correo);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Canales', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Telefono, $Correo, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista los Canales
     */
    public function frmListado(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultarCanales();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Canales', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar canal.
     * @throws NeuralException
     */
    public function frmAgregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']){
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Nombre', '* Campo Requerido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarCanal'));
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Canales', 'Agregar', 'frmAgregar.html')));
            unset($Validacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * Agregar()
     *
     * Funcion de agregar Canal
     * @throws NeuralException
     */
    public function Agregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $this->Modelo->GuardaCanal($DatosPost);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Canales', 'Agregar', 'Exito.html')));
                unset($DatosPost, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * frmEditar()
     *
     * Formulario para editar Canal.
     * @throws NeuralException
     */
    public function frmEditar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdCanal']) == true AND $_POST['IdCanal'] != '') {
                $IdCanal = NeuralCriptografia::DeCodificar($_POST['IdCanal'], APP);
                $Consulta = $this->Modelo->ConsultarCanales(array('tbl_canales.IdCanal' => $IdCanal));
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('Nombre', '* Campo Requerido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarCanal'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Canales', 'Editar', 'frmEditar.html')));
                unset($IdCanal, $Consulta, $Validacion, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Funcion de editar Canal
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $IdCanal = NeuralCriptografia::DeCodificar($_POST['IdCanal'], APP);
                unset($_POST['IdCanal'],$_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $this->Modelo->EditarDatos($DatosPost, $IdCanal);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Canales', 'Agregar', 'Exito.html')));
                unset($IdCanal, $DatosPost, $Plantilla);
                exit();
            }
        }
    }
}