<?php

class Perfil extends Controlador {

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct() {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index() {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Telefono = $this->Informacion['Informacion']['Telefono'];
        $Correo = $this->Informacion['Informacion']['Correo'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        $Plantilla->Parametro('Telefono', $Telefono);
        $Plantilla->Parametro('Correo', $Correo);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Perfil', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Telefono, $Correo, $Plantilla);
        exit();
    }
    /**
     * Metodo Publico
     * frmPerfil()
     *
     * Formulario los datos del Supervisor.
     * @throws NeuralException
     */
    public function frmPerfil(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($this->Informacion['Informacion']) == true AND $this->Informacion['Informacion'] != '') {
                $Consulta[0]=$this->Informacion['Informacion'];
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('Nombres', '* Campo Requerido');
                $Validacion->Requerido('ApellidoPaterno', '* Campo Requerido');
                $Validacion->Requerido('ApellidoMaterno', '* Campo Requerido');
                $Validacion->Requerido('Telefono', '* Campo Requerido');
                $Validacion->Requerido('Usuario', '* Campo Requerido');
                $Validacion->Requerido('Correo', '* Campo Requerido');
                $Validacion->Requerido('RepiteCorreo', '* Campo Requerido');
                $Validacion->CampoIgual('RepiteCorreo','Correo');
                $Validacion->Email('Correo', '* Email formato invalido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarSupervisor'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Perfil', 'Editar', 'frmEditar.html')));
                unset($Consulta, $Validacion, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Funcion de editar los datos del supervisor
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                unset($_POST['RepiteCorreo'], $_POST['Key'], $_POST['IdUsuario']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $Password=$DatosPost['Password'];
                if($Password != null)
                    $DatosLogueo = array('Usuario' => $DatosPost['Usuario'], 'Password' =>  hash('sha256',$Password));
                else
                    $DatosLogueo = array('Usuario' => $DatosPost['Usuario']);
                unset($DatosPost['Usuario'], $DatosPost['Password']);
                $this->Modelo->EditarUsuario($IdUsuario, $DatosLogueo, $DatosPost);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Perfil', 'Editar', 'Exito.html')));
                unset($IdUsuario, $DatosPost, $DatosLogueo, $Password, $Plantilla);
                exit();
            }
        }
    }

}