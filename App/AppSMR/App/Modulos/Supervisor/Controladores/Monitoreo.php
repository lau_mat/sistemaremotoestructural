<?php

class Monitoreo extends Controlador
{

    var $Informacion;
    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Telefono = $this->Informacion['Informacion']['Telefono'];
        $Correo = $this->Informacion['Informacion']['Correo'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        $Plantilla->Parametro('Telefono', $Telefono);
        $Plantilla->Parametro('Correo', $Correo);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Monitoreo', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Telefono, $Correo, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista las Instrumentaciónes
     */
    public function frmListado(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdInformacion = $this->Informacion['Informacion']['IdInformacion'];
            $Consulta = $this->Modelo->ConsultarInstrumentacion($IdInformacion, false);
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            $Plantilla->Filtro('ascii_hex', function ($Parametro) {
                return AppConversores::ASCII_HEX($Parametro);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Monitoreo', 'Listado', 'Listado.html')));
            unset($Consulta, $IdInformacion, $Plantilla);
            exit();
        }
    }

    /**
     *Metodo Publico
     * DataJson
     *
     * Datos json para grafica de inscripciones a los talleres
     **/
    public function DataJson(){
        $IdInstrumentacion = $_GET['IdInstrumentacion'];
        $IdInstrumentacion = AppConversores::HEX_ASCII($IdInstrumentacion);
        $IdInstrumentacion = NeuralCriptografia::DeCodificar($IdInstrumentacion, APP);
        $IdInformacion = $this->Informacion['Informacion']['IdInformacion'];
        $Consulta = $this->Modelo->ConsultarInstrumentacion($IdInformacion, array('tbl_instrumentacion.IdInstrumentacion'=>$IdInstrumentacion));
        $IdDetalleInstrumentacion=$Consulta[0]['IdDetalleInstrumentacion'];
        $ConsultaCanales=$this->Modelo->ConsultarCanalesActivos($IdDetalleInstrumentacion);
        $setAllChannel = Array();
        unset($IdInstrumentacion, $IdInformacion, $Consulta, $IdDetalleInstrumentacion);
        if(0 <count($ConsultaCanales)){
            $dataSet['label'] = $ConsultaCanales[0]['Nombre'];
            $CanalAux=$this->Modelo->ConsultarActivida($ConsultaCanales[0]['IdCanalActivo']);
            $CanalAux = array_reverse($CanalAux);
            $dataSet['data'] = Array();
            for($i=0; $i<count($CanalAux);$i++){
                array_push($dataSet['data'],Array($CanalAux[$i]['FechaHora'],$CanalAux[$i]['Registro']));
            }
            array_push($setAllChannel, $dataSet);
        }
        if(1 <count($ConsultaCanales)){
            $dataSet['label'] =  $ConsultaCanales[1]['Nombre'];
            $CanalAux=$this->Modelo->ConsultarActivida($ConsultaCanales[1]['IdCanalActivo']);
            $CanalAux = array_reverse($CanalAux);
            $dataSet['data'] = Array();
            for($i=0; $i<count($CanalAux);$i++){
                array_push($dataSet['data'],Array($CanalAux[$i]['FechaHora'],$CanalAux[$i]['Registro']));
            }
            array_push($setAllChannel, $dataSet);
        }
        if(2 <count($ConsultaCanales)){
            $dataSet['label'] = $ConsultaCanales[2]['Nombre'];
            $CanalAux=$this->Modelo->ConsultarActivida($ConsultaCanales[2]['IdCanalActivo']);
            $CanalAux = array_reverse($CanalAux);
            $dataSet['data'] = Array();
            for($i=0; $i<count($CanalAux);$i++){
                array_push($dataSet['data'],Array($CanalAux[$i]['FechaHora'],$CanalAux[$i]['Registro']));
            }
            array_push($setAllChannel, $dataSet);
        }
        if(3 <count($ConsultaCanales)){
            $dataSet['label'] = $ConsultaCanales[3]['Nombre'];
            $CanalAux=$this->Modelo->ConsultarActivida($ConsultaCanales[3]['IdCanalActivo']);
            $CanalAux = array_reverse($CanalAux);
            $dataSet['data'] = Array();
            for($i=0; $i<count($CanalAux);$i++){
                array_push($dataSet['data'],Array($CanalAux[$i]['FechaHora'],$CanalAux[$i]['Registro']));
            }
            array_push($setAllChannel, $dataSet);
        }
        if(4 <count($ConsultaCanales)){
            $dataSet['label'] = $ConsultaCanales[4]['Nombre'];
            $CanalAux=$this->Modelo->ConsultarActivida($ConsultaCanales[4]['IdCanalActivo']);
            $CanalAux = array_reverse($CanalAux);
            $dataSet['data'] = Array();
            for($i=0; $i<count($CanalAux);$i++){
                array_push($dataSet['data'],Array($CanalAux[$i]['FechaHora'],$CanalAux[$i]['Registro']));
            }
            array_push($setAllChannel, $dataSet);
        }
        echo json_encode($setAllChannel, JSON_NUMERIC_CHECK);
        unset($setAllChannel, $dataSet, $ConsultaCanales, $CanalAux, $i);
    }

    /**
     * Metodo Publico
     * Graficar()
     *
     * Lista las Instrumentaciónes
     */
    public function Graficar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdInstrumentacion=NeuralCriptografia::DeCodificar($_POST['IdInstrumentacion'] , APP);
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('IdInstrumentacion', $IdInstrumentacion);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            $Plantilla->Filtro('ascii_hex', function ($Parametro) {
                return AppConversores::ASCII_HEX($Parametro);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Monitoreo', 'Graficar', 'Graficar.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * Excel()
     *
     * Exporta datos en Excel
     */
    public function Excel($IdInstrumentacion = false){
        if ($IdInstrumentacion!=false){
            $IdInstrumentacion=AppConversores::HEX_ASCII($IdInstrumentacion);
            $IdInstrumentacion= NeuralCriptografia::DeCodificar($IdInstrumentacion, APP);
            $IdInformacion = $this->Informacion['Informacion']['IdInformacion'];
            $Consulta = $this->Modelo->ConsultarInstrumentacion($IdInformacion, array('tbl_instrumentacion.IdInstrumentacion'=>$IdInstrumentacion));
            $IdDetalleInstrumentacion=$Consulta[0]['IdDetalleInstrumentacion'];
            $ConsultaCanales=$this->Modelo->ConsultarCanalesActivos($IdDetalleInstrumentacion);
            $Canales = array('Muestra n°', 'Tiempo');
            $lista=array();
            for ($i = 0; $i < count($ConsultaCanales); $i++) {
                array_push($Canales,$ConsultaCanales[$i]['Nombre']);
            }
            $CanalAux1=0;
            $CanalAux2=0;
            $CanalAux3=0;
            $CanalAux4=0;
            $CanalAux5=0;
            if(0 <count($ConsultaCanales)){
                $CanalAux1=$this->Modelo->ConsultarActivida($ConsultaCanales[0]['IdCanalActivo']);
            }
            if(1 < count($ConsultaCanales)){
                $CanalAux2=$this->Modelo->ConsultarActivida($ConsultaCanales[1]['IdCanalActivo']);
            }
            if(2 <count($ConsultaCanales)){
                $CanalAux3=$this->Modelo->ConsultarActivida($ConsultaCanales[2]['IdCanalActivo']);
            }
            if(3 < count($ConsultaCanales)){
                $CanalAux4=$this->Modelo->ConsultarActivida($ConsultaCanales[3]['IdCanalActivo']);
            }
           if(4 < count($ConsultaCanales)){
                $CanalAux5=$this->Modelo->ConsultarActivida($ConsultaCanales[4]['IdCanalActivo']);
            }

            for($i=0; $i<count($CanalAux1);$i++){
                array_push($lista,array());
                array_push($lista[$i], $i+1);
                array_push($lista[$i], $CanalAux1[$i]['FechaHora']);
                array_push($lista[$i],$CanalAux1[$i]['Registro']);

                if(1 < count($ConsultaCanales)){
                    array_push($lista[$i],$CanalAux2[$i]['Registro']);
                }
                if(2 <count($ConsultaCanales)){
                    array_push($lista[$i],$CanalAux3[$i]['Registro']);
                }
                if(3 < count($ConsultaCanales)){
                    array_push($lista[$i],$CanalAux4[$i]['Registro']);
                }
                if(4 < count($ConsultaCanales)){
                    array_push($lista[$i],$CanalAux5[$i]['Registro']);
                }
            }
            unset($CanalAux1,$CanalAux2,$CanalAux3,$CanalAux4,$CanalAux5, $ConsultaCanales, $IdInstrumentacion, $IdInformacion, $IdDetalleInstrumentacion);
            unset($Consulta[0]['IdInstrumentacion'], $Consulta[0]['IdSensor'], $Consulta[0]['Status'], $Consulta[0]['IdDetalleInstrumentacion']);
            header('Content-Type:text/csv; charset=UTF-8');
            header('Content-Disposition: attachment; filename="descarga.csv"');
            $salida=fopen('php://output','w');
            fputcsv($salida,  array('Informacion de la instrumentacion'));
            fputcsv($salida,  array('NombreInstrumentacion', 'Direccion', 'Descripcion', 'Mac del sensor', 'SensorNombre', 'PerfilActual'));
            fputcsv($salida,  $Consulta[0]);
            fputcsv($salida,  array(''));
            fputcsv($salida,  $Canales);
            foreach ($lista as $campos) {
                fputcsv($salida,$campos);
            }
            unset($salida,$Canales,$Consulta, $lista);
        }
    }
}