<?php

class Instrumentaciones extends Controlador {

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct() {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index() {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Telefono = $this->Informacion['Informacion']['Telefono'];
        $Correo = $this->Informacion['Informacion']['Correo'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        $Plantilla->Parametro('Telefono', $Telefono);
        $Plantilla->Parametro('Correo', $Correo);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Telefono, $Correo, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista las Instrumentaciónes
     */
    public function frmListado(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdInformacion=$this->Informacion['Informacion']['IdInformacion'];
            $Consulta = $this->Modelo->ConsultarInstrumentacion($IdInformacion, false);
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Listado', 'Listado.html')));
            unset($Consulta, $IdInformacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar Instrumentacion.
     * @throws NeuralException
     */
    public function frmAgregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdInformacion=$this->Informacion['Informacion']['IdInformacion'];
            $Supervisores = $this->Modelo->ConsultarSupervisores($IdInformacion, false);
            $Sensores = $this->Modelo->ConsultarSensores();
            $Canales = $this->Modelo->ConsultarCanales();
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Nombre', '* Campo Requerido');
            $Validacion->Requerido('Direccion', '* Campo Requerido');
            $Validacion->Requerido('Descripcion', '* Campo Requerido');
            $Validacion->Requerido('IdSensor', '* Campo Requerido');
             $Validacion->CantMinCaracteres('Descripcion',100, '* Minimo 100 caracteres');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Supervisores', $Supervisores);
            $Plantilla->Parametro('Sensores', $Sensores);
            $Plantilla->Parametro('Canales', $Canales);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarInstrumentacion'));
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Agregar', 'frmAgregar.html')));
            unset($IdInformacion, $Supervisores, $Sensores, $Canales, $Validacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * Agregar()
     *
     * Funcion de agregar Instrumentacion
     * @throws NeuralException
     */
    public function Agregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                if (AppPost::DatosVaciosOmitidos($_POST, array('IdAuxiliares', 'IdCanal', 'IdSensor'))== false) {
                    if (isset($_POST['IdAuxiliares']) == true)
                        $IdAuxiliares = $_POST['IdAuxiliares'];
                    if (isset($_POST['IdSensor']) == true)
                        $IdSensor = $_POST['IdSensor'];
                    if (isset($_POST['IdCanal']) == true)
                        $IdCanales = $_POST['IdCanal'];

                    unset($_POST['Key'], $_POST['IdAuxiliares'], $_POST['IdCanal'], $_POST['IdSensor']);
                    $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                    $IdInstrumentacion = $this->Modelo->GuardarDatos($DatosPost);
                    $IdInformacion=$this->Informacion['Informacion']['IdInformacion'];
                    if (isset($IdInstrumentacion) == true) {
                        $DatosEncargado = array(
                            'IdInformacion' => $IdInformacion,
                            'IdInstrumentacion' => $IdInstrumentacion,
                            'Principal' => 'PRINCIPAL');
                        $this->Modelo->GuardarEncargado($DatosEncargado);

                        if (isset($IdAuxiliares) == true AND is_array($IdAuxiliares) == true AND count($IdAuxiliares) > 0) {
                            foreach ($IdAuxiliares as $IdAuxiliar) {
                                $this->Modelo->GuardarEncargado(array(
                                    'IdInformacion' => NeuralCriptografia::DeCodificar($IdAuxiliar, APP),
                                    'IdInstrumentacion' => $IdInstrumentacion,
                                    'Principal' => 'AUXILIAR'));
                            }
                        }
                        if(isset($IdSensor) == true){
                         $DetalleInstrumentacion =  $this->Modelo->GuardarSensor(array(
                                'IdSensor' => NeuralCriptografia::DeCodificar($IdSensor, APP),
                                'IdInstrumentacion' => $IdInstrumentacion));
                            if (isset($IdCanales) == true AND is_array($IdCanales) == true AND count($IdCanales) > 0) {
                                foreach ($IdCanales as $IdCanal) {
                                    $this->Modelo->GuardarCanal(array(
                                        'IdCanal' => NeuralCriptografia::DeCodificar($IdCanal, APP),
                                        'IdDetalleInstrumentacion' => $DetalleInstrumentacion));
                                }
                            }
                        }
                        $Plantilla = new NeuralPlantillasTwig(APP);
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Agregar', 'Exito.html')));
                        unset($IdAuxiliares, $IdSensor, $DatosPost, $IdCanales, $IdInstrumentacion, $IdInformacion, $DatosEncargado, $DetalleInstrumentacion, $Plantilla);
                        exit();
                    } else {
                        $Plantilla = new NeuralPlantillasTwig(APP);
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Error', 'ErrorInsertandoDatos.html')));
                        unset($IdAuxiliares, $IdSensor, $DatosPost, $IdCanales, $IdInstrumentacion, $IdInformacion, $Plantilla);
                        exit();
                    }
                } else {
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Error', 'ErrorElementosRequeridos.html')));
                    unset($Plantilla);
                    exit();
                }
            }
        }
    }

    /**
     * Metodo publico
     * frmEditar()
     *
     * Formulario para Editar Instrumentacion.
     * @throws NeuralException
     */
    public function frmEditar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdInformacion=$this->Informacion['Informacion']['IdInformacion'];
            $IdInstrumentacion =  NeuralCriptografia::DeCodificar($_POST['IdInstrumentacion'], APP);
            $Consulta = $this->Modelo->ConsultarInstrumentacion($IdInformacion, array("tbl_instrumentacion.IdInstrumentacion"=>$IdInstrumentacion));
            $Supervisores = $this->Modelo->ConsultarSupervisores($IdInformacion, false);
            $SupervisoresAuxiliares = $this->Modelo->ConsultarSuperv($IdInstrumentacion);
            $Sensores = $this->Modelo->ConsultarSensores();
            $SensorActivo = $this->Modelo->ConsultarSensorActivo($IdInstrumentacion);
            $Canales = $this->Modelo->ConsultarCanales();
            $CanalesActivo = $this->Modelo->ConsultarCanalesActivos($IdInstrumentacion);
            $Supervisores = AppUtilidades::ObtenerColumnaCoincidencia($Supervisores, $SupervisoresAuxiliares, 'IdInformacion');
            $Sensores = AppUtilidades::ObtenerColumnaCoincidencia($Sensores, $SensorActivo, 'IdSensor');
            $Canales = AppUtilidades::ObtenerColumnaCoincidencia($Canales, $CanalesActivo, 'IdCanal');
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Nombre', '* Campo Requerido');
            $Validacion->Requerido('Direccion', '* Campo Requerido');
            $Validacion->Requerido('Descripcion', '* Campo Requerido');
            $Validacion->Requerido('IdSensor', '* Campo Requerido');
            $Validacion->CantMinCaracteres('Descripcion',100, '* Minimo 100 caracteres');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('IdInstrumentacion', $IdInstrumentacion);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Parametro('Supervisores', $Supervisores);
            $Plantilla->Parametro('Sensores', $Sensores);
            $Plantilla->Parametro('Canales', $Canales);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarInstrumentacion'));
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Editar', 'frmEditar.html')));
            unset($Consulta, $IdInstrumentacion, $IdInformacion, $Supervisores, $SupervisoresAuxiliares, $Sensores, $SensorActivo, $Canales, $CanalesActivo, $Validacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Funcion de Editar Instrumentacion
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                if (AppPost::DatosVaciosOmitidos($_POST, array('IdAuxiliares', 'IdCanal', 'IdSensor'))== false) {
                    $IdInformacion=$this->Informacion['Informacion']['IdInformacion'];
                    $IdInstrumentacion =  NeuralCriptografia::DeCodificar($_POST['IdInstrumentacion'], APP);
                    $IdDetalleInstrumentacion =  NeuralCriptografia::DeCodificar($_POST['IdDetalleInstrumentacion'], APP);
                    if (isset($_POST['IdAuxiliares']) == true)
                        $IdAuxiliares = $_POST['IdAuxiliares'];
                    if (isset($_POST['IdSensor']) == true)
                        $IdSensor = $_POST['IdSensor'];
                    if (isset($_POST['IdCanal']) == true)
                        $IdCanales = $_POST['IdCanal'];
                    $this->Modelo->EliminarAuxiliares($IdInstrumentacion);
                    $this->Modelo->EliminarSensor($IdInstrumentacion, $IdDetalleInstrumentacion);
                    unset($_POST['Key'], $_POST['IdAuxiliares'], $_POST['IdCanal'], $_POST['IdSensor'], $_POST['IdInstrumentacion'], $_POST['IdDetalleInstrumentacion']);
                    $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                    $IdInstrumentacionEditar = $this->Modelo->EditarDatos($DatosPost, $IdInstrumentacion);
                    if (isset($IdInstrumentacionEditar) == true) {
                        if (isset($IdAuxiliares) == true AND is_array($IdAuxiliares) == true AND count($IdAuxiliares) > 0) {
                            foreach ($IdAuxiliares as $IdAuxiliar) {
                                $this->Modelo->GuardarEncargado(array(
                                    'IdInformacion' => NeuralCriptografia::DeCodificar($IdAuxiliar, APP),
                                    'IdInstrumentacion' => $IdInstrumentacion,
                                    'Principal' => 'AUXILIAR'));
                            }
                        }
                        if(isset($IdSensor) == true){
                            $DetalleInstrumentacion =  $this->Modelo->GuardarSensor(array(
                                'IdSensor' => NeuralCriptografia::DeCodificar($IdSensor, APP),
                                'IdInstrumentacion' => $IdInstrumentacion));
                            if (isset($IdCanales) == true AND is_array($IdCanales) == true AND count($IdCanales) > 0) {
                                foreach ($IdCanales as $IdCanal) {
                                    $this->Modelo->GuardarCanal(array(
                                        'IdCanal' => NeuralCriptografia::DeCodificar($IdCanal, APP),
                                        'IdDetalleInstrumentacion' => $DetalleInstrumentacion));
                                }
                            }
                        }
                        $Plantilla = new NeuralPlantillasTwig(APP);
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Agregar', 'Exito.html')));
                        unset($IdAuxiliares, $IdInstrumentacionEditar, $IdSensor, $DatosPost, $IdCanales, $IdInstrumentacion, $IdInformacion, $DatosEncargado, $DetalleInstrumentacion, $IdDetalleInstrumentacion, $Plantilla);
                        exit();
                    } else {
                        $Plantilla = new NeuralPlantillasTwig(APP);
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Error', 'ErrorInsertandoDatos.html')));
                        unset($IdAuxiliares, $IdInstrumentacionEditar, $IdSensor, $DatosPost, $IdCanales, $IdInstrumentacion, $IdDetalleInstrumentacion, $IdInformacion, $Plantilla);
                        exit();
                    }
                } else {
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instrumentaciones', 'Error', 'ErrorElementosRequeridos.html')));
                    unset($Plantilla);
                    exit();
                }
            }
        }
    }


    /**
     * Metodo Publico
     * EliminarRegistro()
     *
     * Recibe el arreglo post con el id del Instrumentacion
     * y cambia su Status a "ELIMINADO"
     */
    public function EliminarRegistro(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND $_POST['IdInstrumentacion'] != "") {
                $IdInstrumentacion = NeuralCriptografia::DeCodificar($_POST['IdInstrumentacion'], APP);
                $this->Modelo->Eliminar($IdInstrumentacion);
            }
        }
    }

}