<?php
class Instrumentaciones_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarInstrumentacion()
     *
     * Devuelve las Instrumentaciones Realizadas.
     * @return mixed
     */
    public function ConsultarInstrumentacion($IdInformacion = false, $Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_instrumentacion',false, false, APP));
        $Campos.= ', '.implode(',', self::ListarColumnas('tbl_sensores', array('Status'),array('Nombre'=>'SensorNombre'), APP));
        $Campos.=', Principal, tbl_detalle_instrumentacion.IdDetalleInstrumentacion';
        $SQL = "SELECT $Campos FROM tbl_instrumentacion";
        $SQL.=" INNER JOIN tbl_detalle_instrumentacion ON  tbl_instrumentacion.IdInstrumentacion = tbl_detalle_instrumentacion.IdInstrumentacion";
        $SQL.=" INNER JOIN tbl_sensores ON tbl_detalle_instrumentacion.IdSensor = tbl_sensores.IdSensor";
        $SQL.=" INNER JOIN tbl_encargados ON tbl_instrumentacion.IdInstrumentacion = tbl_encargados.IdInstrumentacion ";
        $SQL.=' WHERE tbl_instrumentacion.status != "ELIMINADO" AND tbl_detalle_instrumentacion.status != "ELIMINADO" AND tbl_encargados.IdInformacion = '.$IdInformacion;
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' AND '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarSupervisores()
     *
     * Devuelve los Supervisores Activos.
     * @return mixed
     */
    public function ConsultarSupervisores($IdInformacion = false, $Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_informacion_usuarios', array('IdUsuario', 'Status'), false, APP));
        $Campos.= ', '.implode(',', self::ListarColumnas('tbl_sistema_usuarios', array('IdPerfil', 'Password'),false, APP));
        $SQL = "SELECT $Campos FROM tbl_informacion_usuarios ";
        $SQL.=" INNER JOIN tbl_sistema_usuarios ON  tbl_informacion_usuarios.IdUsuario = tbl_sistema_usuarios.IdUsuario";
        $SQL.=' WHERE tbl_sistema_usuarios.status != "ELIMINADO" AND tbl_sistema_usuarios.IdPerfil != 1 AND tbl_informacion_usuarios.IdInformacion != '.$IdInformacion;
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' AND '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

     /**
     * Metodo Publico
     * ConsultarSuperv()
     *
     * Devuelve los SupervisoresAuxiliares ligados a la instrumentacion.
     * @return mixed
     */
    public function ConsultarSuperv( $Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_encargados', false, false, APP));
        $SQL = "SELECT $Campos FROM tbl_encargados";
        $SQL.=' WHERE tbl_encargados.Principal = "AUXILIAR"';
        if($Condiciones == true)
            $SQL.=' AND tbl_encargados.IdInstrumentacion = '.$Condiciones;
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarSensores()
     *
     * Devuelve los Sensores Activos.
     * @return mixed
     */
    public function ConsultarSensores($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_sensores', false, false, APP));
        $SQL = "SELECT $Campos FROM tbl_sensores ";
        $SQL.=' where status = "ACTIVO" ';
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' and '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarSensorActivo()
     *
     * Devuelve los Sensores Activos.
     * @return mixed
     */
    public function ConsultarSensorActivo($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_detalle_instrumentacion', false, false, APP));
        $SQL = "SELECT $Campos FROM tbl_detalle_instrumentacion ";
        $SQL.=' where status = "ACTIVO" ';
        if($Condiciones == true)
            $SQL.=' AND tbl_detalle_instrumentacion.IdInstrumentacion = '.$Condiciones;
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarCanales()
     *
     * Devuelve los canales.
     * @return mixed
     */
    public function ConsultarCanales($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_canales', false, false, APP));
        $SQL = "SELECT $Campos FROM tbl_canales ";
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' where '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarCanalesActivos()
     *
     * Devuelve los canales vinvulado la Instrumentacion.
     * @return mixed
     */
    public function ConsultarCanalesActivos($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_canales_activos', false, false, APP));
        $SQL = "SELECT $Campos FROM tbl_canales_activos ";
        $SQL.=" INNER JOIN tbl_detalle_instrumentacion ON  tbl_canales_activos.IdDetalleInstrumentacion = tbl_detalle_instrumentacion.IdDetalleInstrumentacion";
        if($Condiciones == true)
            $SQL.=' where tbl_canales_activos.status = "ACTIVO" and tbl_detalle_instrumentacion.IdInstrumentacion = '.$Condiciones;
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * GuardarDatos($Datos = false)
     *
     * Guarda una instrumentacion
     * @param bool $Dato
     * @return mixed
     */
    public function GuardarDatos($Datos = false){
        if ($Datos == true AND is_array($Datos) == true) {
            try {
                $this->Conexion->insert('tbl_instrumentacion', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * GuardarEncargado($Datos = false)
     *
     * Guarda el encargado y auxiliares del monitoreo
     * @param bool $Dato
     * @return mixed
     */
    public function GuardarEncargado($Datos = false){
        if ($Datos == true AND is_array($Datos) == true) {
            try {
                $this->Conexion->insert('tbl_encargados', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * GuardarSensor($Datos = false)
     *
     * Guarda el sensor vinvulado al espesimen a instrumentar
     * @param bool $Dato
     * @return mixed
     */
    public function GuardarSensor($Datos = false){
        if ($Datos == true AND is_array($Datos) == true) {
            try {
                $this->Conexion->insert('tbl_detalle_instrumentacion', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * GuardarCanal($Datos = false)
     *
     * Guarda el sensor vinvulado al espesimen a instrumentar
     * @param bool $Dato
     * @return mixed
     */
    public function GuardarCanal($Datos = false){
        if ($Datos == true AND is_array($Datos) == true) {
            try {
                $this->Conexion->insert('tbl_canales_activos', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * EditarDatos($Datos = false, $IdInstrumentacion = false)
     *
     * Editar datos del Sensor
     * @param bool $Datos
     * @param bool $IdInstrumentacion
     */
    public function EditarDatos($Datos = false, $IdInstrumentacion = false){
        if($Datos == true AND is_array($Datos) == true AND $IdInstrumentacion == true AND $IdInstrumentacion != ''){
            try{
                return $this->Conexion->update('tbl_instrumentacion', $Datos, array('IdInstrumentacion'=>$IdInstrumentacion));
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * @param bool $IdInstrumentacion
     *
     * Metodo Publico Eliminar Instrumentacion
     * Cambia el Status de la Instrumentacion asociado al id
     * por "ELIMINADO"
     */
    public function Eliminar($IdInstrumentacion = false){
        if($IdInstrumentacion == true AND $IdInstrumentacion != ''){
            try{
                $this->Conexion->update('tbl_instrumentacion',array('Status'=>"ELIMINADO"), array('IdInstrumentacion'=>$IdInstrumentacion));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    /**
     * @param bool $IdInstrumentacion
     *
     * Metodo Publico Eliminar Auxiliares
     * Cambia el Status de la Instrumentacion asociado al id
     * por "ELIMINADO"
     */
    public function EliminarAuxiliares($IdInstrumentacion = false){
        if($IdInstrumentacion == true AND $IdInstrumentacion != ''){
            try{
                $this->Conexion->delete('tbl_encargados', array('IdInstrumentacion'=>$IdInstrumentacion, 'Principal'=>'AUXILIAR'));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    /**
     * @param bool $IdInstrumentacion
     *
     * Metodo Publico Eliminar Sensor vinculado a instrumentacion junto con sus canales
     * Cambia el Status de la Instrumentacion asociado al id
     * por "ELIMINADO"
     */
    public function EliminarSensor($IdInstrumentacion = false, $IdDetalleInstrumentacion = false){
        if($IdInstrumentacion == true AND $IdInstrumentacion != '' AND $IdDetalleInstrumentacion == true AND $IdDetalleInstrumentacion != ''){
            try{
                $this->Conexion->update('tbl_canales_activos',array('Status'=>"ELIMINADO"), array('IdDetalleInstrumentacion'=>$IdDetalleInstrumentacion));
                $this->Conexion->update('tbl_detalle_instrumentacion',array('Status'=>"ELIMINADO"), array('IdInstrumentacion'=>$IdInstrumentacion));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }
}