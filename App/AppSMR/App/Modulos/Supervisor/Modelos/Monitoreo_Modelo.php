<?php
class Monitoreo_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarInstrumentacion()
     *
     * Devuelve las Instrumentaciones Realizadas.
     * @return mixed
     */
    public function ConsultarInstrumentacion($IdInformacion = false, $Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_instrumentacion',false, false, APP));
        $Campos.= ', '.implode(',', self::ListarColumnas('tbl_sensores', array('Status'),array('Nombre'=>'SensorNombre'), APP));
        $Campos.=', Principal, tbl_detalle_instrumentacion.IdDetalleInstrumentacion';
        $SQL = "SELECT $Campos FROM tbl_instrumentacion";
        $SQL.=" INNER JOIN tbl_detalle_instrumentacion ON  tbl_instrumentacion.IdInstrumentacion = tbl_detalle_instrumentacion.IdInstrumentacion";
        $SQL.=" INNER JOIN tbl_sensores ON tbl_detalle_instrumentacion.IdSensor = tbl_sensores.IdSensor";
        $SQL.=" INNER JOIN tbl_encargados ON tbl_instrumentacion.IdInstrumentacion = tbl_encargados.IdInstrumentacion ";
        $SQL.=' WHERE tbl_instrumentacion.status != "ELIMINADO" AND tbl_detalle_instrumentacion.status != "ELIMINADO" AND tbl_encargados.IdInformacion = '.$IdInformacion;
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.=' AND '.self::ObtenerCondicionesAND($Condiciones);
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarCanalesActivos()
     *
     * Devuelve los canales vinvulado la Instrumentacion.
     * @return mixed
     */
    public function ConsultarCanalesActivos($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_canales_activos', array('IdCanal', 'Status', 'IdDetalleInstrumentacion'), false, APP));
        $Campos.= ', '.implode(',', self::ListarColumnas('tbl_canales', array('IdCanal'),false, APP));
        $SQL = "SELECT $Campos FROM tbl_canales_activos ";
        $SQL.=" INNER JOIN tbl_canales ON  tbl_canales_activos.IdCanal = tbl_canales.IdCanal";
        if($Condiciones == true)
            $SQL.=' where tbl_canales_activos.status = "ACTIVO" and tbl_canales_activos.IdDetalleInstrumentacion = '.$Condiciones;
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarActivida()
     *
     * Devuelve los registro del canal Activo.
     * @return mixed
     */
    public function ConsultarActivida($Condiciones = false){
        $Campos = implode(',', self::ListarColumnas('tbl_activida', array('IdActivida', 'IdCanalActivo'), false, APP));
        $SQL = "SELECT $Campos FROM tbl_activida ";
        if($Condiciones == true)
            $SQL.=' where tbl_activida.IdCanalActivo = '.$Condiciones.' Order by FechaHora DESC LIMIT 8';
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }
}