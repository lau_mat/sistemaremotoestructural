<?php
class Perfil_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * @param bool $IdUsuario = false, $DatosUsuario = false, $Datos = false
     *
     * Metodo Publico EditarUsuario
     * Actualiza los datos del usuario asociado al id
     */
    public function EditarUsuario($IdUsuario = false, $DatosUsuario = false, $Datos = false)
    {
        if ($IdUsuario == true AND $IdUsuario != '' AND $Datos == true AND $Datos != '' AND $DatosUsuario == true AND $DatosUsuario != '') {
            try {
                $this->Conexion->update('tbl_sistema_usuarios', $DatosUsuario, array('IdUsuario' => $IdUsuario));
                $this->Conexion->update('tbl_informacion_usuarios', $Datos, array('IdUsuario' => $IdUsuario));
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }
}