<?php

	class SinAutorizacion extends Controlador {

		function __Construct() {
			parent::__Construct();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Mustra la pantalla de error sin acceso
		 *
		 */
		public function Index() {
			$Plantilla = new NeuralPlantillasTwig(APP);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('SinAutorizacion', 'NoExisteUsuario.html')));
            unset($Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * CorreoEnUso()
		 *
		 * Muestra la pantalla de usuario con correco en uso
		 *
		 * */
		public function CorreoEnUso() {
			$Plantilla = new NeuralPlantillasTwig(APP);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('SinAutorizacion', 'CorreoUsado.html')));
			unset($Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * PasswordnotSame()
		 *
		 * Muestra la pantalla Password no coinciden
		 *
		 * */
		public function PasswordnotSame() {
			$Plantilla = new NeuralPlantillasTwig(APP);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('SinAutorizacion', 'PasswordNotSame.html')));
			unset($Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * PasswordnotSame()
		 *
		 * Muestra la pantalla Password no coinciden
		 *
		 * */
		public function SinDatos() {
			$Plantilla = new NeuralPlantillasTwig(APP);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('SinAutorizacion', 'SinDatos.html')));
			unset($Plantilla);
			exit();
		}




	}