<?php

	class Exito extends Controlador {

		function __Construct(){
			parent::__Construct();
		}

		public function Index(){
		   $Plantilla = new NeuralPlantillasTwig(APP);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Autorizado', 'RegistroExitoso.html')));
			unset($Plantilla);
			exit();
		}


	}