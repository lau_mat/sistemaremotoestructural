<?php

/**
 * Clase: Index
 */
    class DataProcessing extends Controlador {

    /**
     * Metodo: Constructor
     */
    function __Construct() {
        parent::__Construct();
    }

    /**
     * Metodo: Index
     *  procesa los datos del sensor y almacena en DB
     */
    public function Index() {
        if(isset($_GET) == true && $_GET['Mac']){
            $Mac=$_GET['Mac'];
            $Entrada=$_GET['Entrada'];
            $IdDetalleInst=$this->Modelo->BuscaInstrumentacion($Mac);
            $IdCanalesActivos=$this->Modelo->BuscaIdCanalesActivos($IdDetalleInst[0]['IdDetalleInstrumentacion']);
            $Entrada = explode(" ", $Entrada);
            $FechaHora=AppFechas::ObtenerDatetimeActual();

            if($IdCanalesActivos['Cantidad']>0){
                if($Entrada[0]!=null){
                    $this->Modelo->InsertarRegistro($IdCanalesActivos[0]['IdCanalActivo'], $Entrada[0], $FechaHora);
                }

            }

            if($IdCanalesActivos['Cantidad']>1){
                if($Entrada[1]!=null) {
                    $this->Modelo->InsertarRegistro($IdCanalesActivos[1]['IdCanalActivo'], $Entrada[1], $FechaHora);
                }
            }

            if($IdCanalesActivos['Cantidad']>2){
                if($Entrada[2]!=null) {
                    $this->Modelo->InsertarRegistro($IdCanalesActivos[2]['IdCanalActivo'], $Entrada[2], $FechaHora);
                }
            }

            if($IdCanalesActivos['Cantidad']>3){
                if($Entrada[3]!=null){
                    $this->Modelo->InsertarRegistro($IdCanalesActivos[3]['IdCanalActivo'], $Entrada[3], $FechaHora);
                }
            }
            unset($Mac, $Entrada, $_GET, $IdDetalleInst, $IdCanalesActivos, $FechaHora);
            exit;
        }
    }
}
