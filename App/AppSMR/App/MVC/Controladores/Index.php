<?php

	/**
	 * Clase: Index
	 */
	class Index extends Controlador {

		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
            NeuralSesiones::Inicializar(APP);
            if(isset($_SESSION, $_SESSION['UOAUTH_APP']) == true){
                header('Location:'.NeuralRutasApp::RutaUrlAppModulo('Control'));
                exit();
            }
		}

		/**
		 * Metodo: Index
		 */
		public function Index() {

			/**
			 * Ejecutando el motor de plantillas
			 * Se muestra la plantilla HTML
			 */
			$this->frmLogin();
		}

        /**
         * Metodo publico
         *
         * Login()
         * Muestra el formulario para logearse al sistema.
         * @throws NeuralException
         */
        public function frmLogin(){
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Usuario', '* Nombre de usuario requerido');
            $Validacion->Requerido('Password', '* Contraseña requerida');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frm_Login'));
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            echo $Plantilla->MostrarPlantilla('Login.html');
            unset($Validacion, $Plantilla);
            exit();
        }

        /**
         * Index::Autenticacion()
         *
         * Genera el proceso de autenticacion
         * @return void
         */
        public function Autenticacion() {
            if(isset($_POST) == true AND isset($_POST['Key']) == true AND NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) :
                $this->AutenticacionDatosVacios();
            else:
                $this->AutenticacionErrorRedireccion("Error","SinAutorizacion","SinDatos");
            endif;
        }

        /**
         * Index::AutenticacionDatosVacios()
         *
         * genera la validacion de datos vacios
         * return ok
         * @return void
         */
        private function AutenticacionDatosVacios() {
            if(AppPost::DatosVacios($_POST) == false):
                $this->AutenticacionConsultarUsuario();
            else:
                $this->AutenticacionErrorRedireccion("Error","SinAutorizacion","SinDatos");
            endif;
        }

        /**
         * Index::AutenticacionConsultarUsuario()
         *
         * Genera la validacion del usuario
         * @return ok
         * @return void
         */
        private function AutenticacionConsultarUsuario() {
            unset($_POST['Key']);
            $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
            $Consulta = $this->Modelo->ConsultarUsuario($DatosPost['Usuario'], hash('sha256', $DatosPost['Password']));
            if($Consulta['Cantidad'] == 1 && $Consulta[0]['Status'] != "DESACTIVADO"):
                $this->AutenticacionConsultaPermisos($Consulta);
            elseif($Consulta['Cantidad'] == 1 && $Consulta[0]['Status'] == "DESACTIVADO"):
                $this->AutenticacionErrorRedireccion('Error', 'SinAcceso');
            else:
                $this->AutenticacionErrorRedireccion('Error', 'SinAutorizacion');
            endif;
            unset($DatosPost, $Consulta);
            exit;
        }

        /**
         * Index::AutenticacionConsultaPermisos()
         *
         * Genera la consulta de los permisos correspondientes
         * @return ok
         * @param bool $Consulta
         * @return void
         */
        private function AutenticacionConsultaPermisos($Consulta = false) {
            if($Consulta == true and is_array($Consulta) == true){
                $ConsultaPermisos = $this->Modelo->ConsultarPermisos($Consulta[0]['IdPerfil']);
                if($ConsultaPermisos['Cantidad'] == 1):
                    AppSession::Registrar($Consulta[0], $ConsultaPermisos[0]);
                    unset($Consulta, $ConsultaPermisos);
                    header("Location: ".NeuralRutasApp::RutaUrlAppModulo('Control', 'Index'));
                    exit();
                else:
                    exit();
                endif;
            }
        }

        /**
         * Metodo publico
         * frmRegistro()
         *
         * Formulario de registro de nuevo asistente
         * @throws NeuralException
         */
        public function frmRegistro(){
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Usuario', '* Usuario requerido');
            $Validacion->Requerido('Nombres', '* Nombre requerido');
            $Validacion->Requerido('ApellidoPaterno', '* Apellido requerido');
            $Validacion->Requerido('ApellidoMaterno', '* Apellido requerido');
            $Validacion->Requerido('Password', '* Contraseña requerida');
            $Validacion->Requerido('RepitePassword', '* Confirmar Contraseña');
            $Validacion->Requerido('Correo', '* Correo requerido');
            $Validacion->Requerido('Telefono', '* Telefono requerido');
            $Validacion->Email('Correo', '* Email formato invalido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmRegistro'));
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Agregar', 'frmAgregar.html')));
            unset($Validacion, $Plantilla);
            exit();
        }

        /**
         * Metodo Publico
         * Registro()
         *
         * Registro de un nuevo usuario(Asistente)
         */
        public function Registro(){
            if(NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()){
                if(AppPost::DatosVacios($_POST) == false){
                    if($_POST['Password'] == $_POST['RepitePassword']){
                        unset($_POST["RepitePassword"]);
                        $DatosSistema = array("Usuario" => $_POST['Usuario'],
                            "Password" => hash('sha256', $_POST['Password']),
                            "IdPerfil" => 2);
                        $InformacionUsuario = array("Nombres" => $_POST['Nombres'],
                            "ApellidoPaterno" => $_POST['ApellidoPaterno'],
                            "ApellidoMaterno" => $_POST['ApellidoMaterno'],
                            "Telefono" => $_POST['Telefono'],
                            "Correo" => $_POST['Correo']
                        );
                        $Consulta = $this->Modelo->ConsultarExistenciaUsuario($_POST['Correo']);
                        if($Consulta['Cantidad'] == 0){
                            $IdUsuario = $this ->Modelo->GuardarUsuario(AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($DatosSistema)));
                            $InformacionUsuario["IdUsuario"] = $IdUsuario;
                            $this ->Modelo->GuardarInformacion(AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($InformacionUsuario)));
                            unset($_POST, $DatosSistema, $InformacionUsuario, $IdUsuario, $Consulta);
                            $this->AutenticacionErrorRedireccion("Error","Exito");
                        }else{
                            unset($_POST, $DatosSistema, $InformacionUsuario, $Consulta);
                            $this->AutenticacionErrorRedireccion("Error","SinAutorizacion","CorreoEnUso");
                            exit();
                        }
                    }
                    unset($_POST);
                    $this->AutenticacionErrorRedireccion("Error","SinAutorizacion","PasswordnotSame");
                    exit();
                }
                $this->AutenticacionErrorRedireccion("Error","SinAutorizacion","SinDatos");
                exit();
            }
            $this->AutenticacionErrorRedireccion("Error","SinAutorizacion","SinDatos");
            exit();
        }



        /**
         * Index::AutenticacionErrorRedireccion()
         *
         * Genera el error de redireccion
         * @return ok
         * @param bool $modulo
         * @param bool $controlador
         * @param bool $metodo
         * @return void
         */
        private function AutenticacionErrorRedireccion($modulo = false, $controlador = false, $metodo = false) {
            header("Location: ".NeuralRutasApp::RutaUrlAppModulo($modulo, $controlador, $metodo));
            exit();
        }
	}