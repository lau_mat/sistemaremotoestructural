<?php
	
	/**
	 * Clase: Index_Modelo
	 */
	class Index_Modelo extends AppSQLConsultas {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
            $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

        /**
         * Metodo Publico
         * ConsultarUsuario($Usuario = false, $Password = false)
         *
         * Consulta los datos del usuario
         * retorna un array asociativo con los datos correspondientes
         * @param $Usuario: username
         * @param $Password: contraseña
         * @return array
         **/
        public function ConsultarUsuario($Usuario = false, $Password = false) {
            if($Usuario == true AND $Password    == true) {
                $Consulta = new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_sistema_usuarios');
                $Consulta->Columnas(array_merge(self::ListarColumnas('tbl_informacion_usuarios', array('Status'), false, APP),
                    self::ListarColumnas('tbl_sistema_usuarios', array('Password'), false, APP)));
                $Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.IdUsuario');
                $Consulta->Condicion("(tbl_sistema_usuarios.Usuario = '$Usuario' OR tbl_informacion_usuarios.Correo = '$Usuario')");
                $Consulta->Condicion("tbl_sistema_usuarios.Password = '$Password'");
                $Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
                return $Consulta->Ejecutar(true, true);
            }
        }

        /**
         * Metodo Publico
         * ConsultarPermisos($IdPerfil = false)
         *
         * Genera la consulta de los datos correspondientes
         * @param $Permiso: Identificador del permiso
         * @return array
         */
        public function ConsultarPermisos($IdPerfil = false) {
            if($IdPerfil == true AND is_numeric($IdPerfil) == true) {
                $Consulta = new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_sistema_usuarios_perfil');
                $Consulta->Columnas(self::ListarColumnas('tbl_sistema_usuarios_perfil', array('IdPerfil', 'Status'), false, APP));
                $Consulta->Condicion("IdPerfil = '$IdPerfil'");
                return $Consulta->Ejecutar(true, true);
            }
        }

        /**
         * Metodo Publico
         *
         * ConsultarExistenciaUsuario($Usuario = false)
         * @param bool $Usuario
         * @return array
         */
        public function ConsultarExistenciaUsuario($Usuario = false){
            if($Usuario == true){
                $Consulta = new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_informacion_usuarios');
                $Consulta->Columnas("Correo");
                $Consulta->Condicion("Correo = '$Usuario'");
                return $Consulta->Ejecutar(true, true);
            }
        }

        /**
         * @param bool $Datos
         * @return mixed
         */
        public function GuardarUsuario($Datos = false){
            if($Datos == true and is_array($Datos)==true){
                $this->Conexion->insert('tbl_sistema_usuarios', $Datos);
                return $this->Conexion->lastInsertId();
            }
        }

        /**
         * @param bool $Datos
         */
        public function GuardarInformacion($Datos = false){
            if($Datos == true and is_array($Datos)==true){
                $this->Conexion->insert('tbl_informacion_usuarios', $Datos);
            }
        }

    }