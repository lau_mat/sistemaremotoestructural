<?php
/**
 * Clase: DataProcessing_Modelo
 */
class DataProcessing_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * InsertarRegistro($IdCanalAct =false, $Registro = false)
     * @param bool $IdCanalAct
     * @param bool $Registro
     *
     * Guarda un nuevo Registro asociado a la Mac
     */
    public function InsertarRegistro($IdCanalAct =false, $Registro = false, $FechaHora = false){
        if($IdCanalAct == true and $IdCanalAct != '' and  $Registro == true and $Registro !=''){

            try{
                $this->Conexion->insert('tbl_activida', array('IdCanalActivo'=>$IdCanalAct, 'Registro'=>$Registro, 'FechaHora'=>$FechaHora));
            } catch (PDOException $e){
            } catch (Exception $e) {

            }
        }
    }

    /**
     * Metodo Publico
     * BuscaInstrumentacion($Mac = false)
     * @param bool $Mac
     *
     * En este metodo retornamos el IdDetalleInstrumentacion que esta relacionado con la Mac
     */
    public function BuscaInstrumentacion($Mac = false){
        if($Mac == true and $Mac != ''){
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_detalle_instrumentacion');
            $Consulta->Columnas("tbl_detalle_instrumentacion.IdDetalleInstrumentacion");
            $Consulta->InnerJoin("tbl_sensores","tbl_detalle_instrumentacion.IdSensor", "tbl_sensores.IdSensor");
            $Consulta->Condicion("tbl_sensores.Mac= '$Mac' and tbl_detalle_instrumentacion.Status!='ELIMINADO'");
            return $Consulta->Ejecutar(false, true);
        }
    }

    /**
     * Metodo Publico
     * BuscaIdCanalesActivos($IdDetalleInst = false)
     * @param bool $IdDetalleInst
     *
     * En este metodo retornamos el IdCanalesActivos que esta relacionado con la Instrumentacion
     */
    public function BuscaIdCanalesActivos($IdDetalleInst = false){
        if($IdDetalleInst == true and $IdDetalleInst != ''){
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_canales_activos');
            $Consulta->Columnas("IdCanalActivo, Nombre");
            $Consulta->InnerJoin("tbl_canales","tbl_canales_activos.IdCanal", "tbl_canales.IdCanal");
            $Consulta->Condicion("IdDetalleInstrumentacion = $IdDetalleInst and tbl_canales_activos.Status='ACTIVO'");
            return $Consulta->Ejecutar(true, true);
        }
    }
}
