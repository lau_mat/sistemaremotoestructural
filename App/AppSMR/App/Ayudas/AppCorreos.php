<?php

	class AppCorreos {
		
		/**
		 * Metodo Publico
		 * Arreglo2Hash($Usuario = false, $Correo = false)
		 * 
		 * Convierte arreglo en una cadena codificada
		 * @param $Usuario: Nombre del usuario
		 * @param $Correo: Correo del usuario.
		 * @return Cadena JSON
		 * 
		 * */
		public static function CadenaHash($Arreglo = false){
			if($Arreglo == true){	
				return AppConversores::ASCII_HEX(NeuralCriptografia::Codificar($Arreglo, APP));					
			}
		}
		
		/**
		 * Metodo Publico
		 * Hash2Arreglo($Hash = false)
		 * 
		 * Convierte cadena codificada json a un arreglo de datos
		 * @param $Hash: Cadena codificada JSON
		 * @return Arreglo
		 * 
		 * */
		public static function HashCadena($Hash = false){
			if($Hash == true){
				return NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($Hash), APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * GeneradorPassword()
		 * 
		 * Genera una contraseña de forma aleatoria
		 * @return Contraseña
		 * 
		 * */
		public static function GeneradorPassword(){
			$Abecedario = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		    $Password = array();
		    $Longitud = strlen($Abecedario) - 1;
		    for ($i = 0; $i < 8; $i++) {
		        $n = rand(0, $Longitud);
		        $Password[] = $Abecedario[$n];
		    }
		    return implode($Password);
		}
		
		/**
		 * Metodo Publico
	 	 * EnviaActivacion($Hash = false, $Correo = false)
	 	 * 
	 	 * Envia correcto con activacion de nueva contraseña
	 	 * @param $Hash: Cadena codificada con la activacion
	 	 * @param $Correo: Correo de envio
	 	 * 
	 	 * */
	 	public static function EnviaActivacion($Hash = false, $CorreoEnviar = false){
		 	if($Hash == true AND $CorreoEnviar == true AND !empty($CorreoEnviar)){
		 		$Mensaje = '.:: ZICOM Group :: Soporte Técnico ::. '."\n\n".
                           'Proceso de recuperación de contraseña o activación de cuenta'."\n\n".
                           'Paso para recuperación ó activación de cuentas.'."\n".
						   '1. De le click en el siguiente URL para su Activación de proceso. '."\n".
						   NeuralRutasApp::RutaUrlApp('Index', 'Activacion', array($Hash))."\n\n".
						   '2. Una vez que le de click al URL nos abre una ventana, damos click al botón Activar Cuenta '."\n\n".
						   '3. Le enviaremos una contraseña para ingreso de su cuenta '."\n\n\n".
						   'Si presente algún problema en el proceso contáctenos:'."\n";
		 		$Correo = new NeuralCorreoSwiftMailer(APP, 3, 'html', 'UTF-8');
				$Correo->Asunto('Recuperación Contraseña o Activación');
				$Correo->EnviarA($CorreoEnviar);
				$Correo->Remitente('.:: ZICOM Group ::.', 'contacto@zicomgroup.com');
				$Correo->MensajeAlternativo($Mensaje);
				$Correo->Mensaje($Mensaje);
				$Correo->EnviarCorreo();
				unset($Mensaje, $Correo);
		 	}
	 	}

		/**
		 * Metodo Publico
		 * EnviaActivacion($Hash = false, $Correo = false)
		 *
		 * Envia correcto con activacion de nueva contraseña
		 * @param $Hash: Cadena codificada con la activacion
		 * @param $Correo: Correo de envio
		 *
		 * */
		public static function EnviaActivacionCuenta($Hash = false, $CorreoEnviar = false){
			if($Hash == true AND $CorreoEnviar == true AND !empty($CorreoEnviar)){
				$Mensaje = '= FIDISOHL 2017 ='."\n\n".
					'Proceso de activación de cuenta'."\n\n".
					'1. Haz click en el siguiente URL. '."\n".
					NeuralRutasApp::RutaUrlApp('Index', 'Activacion', array($Hash))."\n\n".
					'2. Una vez que de click al URL, su cuenta queda activada automáticamente'."\n\n".
					'3. Ingrese a la pantalla de inicio de sesión e introduzca sus datos de usuario'."\n\n\n".
					'Nota: debe verificar su cuenta a en un lapso de 3 días de lo contrario tendrá que realizar el proceso de registro nuevamente'."\n\n\n".
					'Si presenta algún problema en el proceso contáctenos: webmaster@fidisohl.mx'."\n";
				$Correo = new NeuralCorreoSwiftMailer(APP, 3, 'html', 'UTF-8');
				$Correo->Asunto('Activación de cuenta');
				$Correo->EnviarA($CorreoEnviar);
				$Correo->Remitente('FIDiSoHL', 'webmaster@fidisohl.mx');
				$Correo->MensajeAlternativo($Mensaje);
				$Correo->Mensaje($Mensaje);
				$Correo->EnviarCorreo();
				unset($Mensaje, $Correo);
			}
		}
		 
	 	public static function EnviaNuevoPassword($Password = false, $Usuario = false, $CorreoEnviar = false) {
		 	if($Password == true AND $Usuario == true AND $CorreoEnviar == true){
				$Mensaje = '.:: ZICOM Group :: Soporte Técnico ::. '."\n\n".
					'Proceso de recuperación de contraseña o activación de cuenta'."\n\n".
					'Nueva contraseña activada, datos de la nueva cuenta:'."\n".
					'Usuario: '.$Usuario."\n".
					'Nueva Contraseña: '.$Password."\n\n";
				$Correo = new NeuralCorreoSwiftMailer(APP, 3, 'html', 'UTF-8');
				$Correo->Asunto('Recuperación Contraseña');
				$Correo->EnviarA($CorreoEnviar);
				$Correo->Remitente('.:: ZICOM Group ::.', 'contacto@zicomgroup.com');
				$Correo->MensajeAlternativo($Mensaje);
				$Correo->Mensaje($Mensaje);
				$Correo->EnviarCorreo();
				unset($Mensaje, $Correo);
			}
	 	} 
		
	 	/**
		 * Metodo Publico
		 * EnviarNuevoRegistro($Hash = false, $Correo = false)
		 * 
		 * Envia correcto con activacion de nueva cuenta
		 * @param $Hash: Cadena codificada con la activacion
		 * @param $Correo: Correo de envio
		 * 
		 * */
		public static function EnviarNuevoRegistro($Hash = false, $CorreoEnviar = false, $Password = false) {
 			if($CorreoEnviar == true AND !empty($CorreoEnviar)){
		 		$Mensaje = '.:: ZICOM Group :: Soporte Técnico ::. '."\n\n".
                           'Proceso activación de cuenta'."\n\n".
                           'Usuario: '.$CorreoEnviar."\n".
                           'Contraseña: '.$Password."\n\n".
                           'Paso para activación de cuentas.'."\n".
						   '1. De le click en el siguiente URL para su Activación. '."\n".
						   NeuralRutasApp::RutaUrlApp('Index', 'ActivacionNuevaCuenta', array($Hash))."\n".
						   '2. Una vez que le de click al URL nos abre una ventana, damos click al botón Activar Cuenta '."\n\n\n".
						   'Si presente algún problema en el proceso contáctenos:'."\n";
				$Correo = new NeuralCorreoSwiftMailer(APP, 3, 'html', 'UTF-8');
				$Correo->Asunto('Activación de Cuenta');
				$Correo->EnviarA($CorreoEnviar);
				$Correo->Remitente('.:: ZICOM Group ::.', 'contacto@zicomgroup.com');
				$Correo->MensajeAlternativo($Mensaje);
				$Correo->Mensaje($Mensaje);
				$Correo->EnviarCorreo();
				unset($Mensaje, $Correo);
		 	}
		 }
		
	}