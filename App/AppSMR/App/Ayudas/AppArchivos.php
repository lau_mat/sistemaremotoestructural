<?php

class AppArchivos {

    /**
     * Metodo Publico
     * CargarArchivoImagen($Archivo = false, $RutaWeb = false)
     *
     * Guarda una imagen en la carpete web de la aplicacion.
     * @param bool|false $Archivo: Array del archivo
     * @param bool|false $RutaWeb: $RutaWeb = implode(DIRECTORY_SEPARATOR, array('img', 'Reportes'));
     * @return string: $RutaWeb + NombreArchivo.extension.
     */
    public static function CargarArchivoImagen($Archivo = false, $RutaWeb = false){
        if($Archivo == true AND is_array($Archivo) == true AND $RutaWeb == true):
            $Extension = $Archivo['type'];
            if(self::ValidarExtensionImagen($Extension) == true):
                $Nombre = $Archivo['name'];
                $Prefijo = substr(md5(uniqid(rand())), 0, 6);

                $NombreArchivo = implode(DIRECTORY_SEPARATOR, array($Prefijo . "_" . $Nombre));

                $Destino = implode(DIRECTORY_SEPARATOR, array(__SysNeuralFileRootApp__, APP, 'Web'));
                $Destino = $Destino.DIRECTORY_SEPARATOR.$RutaWeb.DIRECTORY_SEPARATOR.$NombreArchivo;
                if(copy($Archivo['tmp_name'], $Destino)):
                    return $RutaWeb.DIRECTORY_SEPARATOR.$NombreArchivo;
                else:
                    return "ERROR";
                endif;
            else:
                return "ERROR";
            endif;
        else:
            return "ERROR";
        endif;
    }

    /**
     * Metodo Publico
     * ValidarExtensionImagen($extension = false)
     *
     * Valida el mime type para una imagen png.
     * @param bool|false $extension
     * @return bool|string
     */
    private static function ValidarExtensionImagen($extension = false){
        if($extension == 'image/png')
            return "png";
        else if($extension == 'image/jpg')
            return "jpg";
        else if($extension == 'image/jpeg')
            return "jpeg";
        else
            return false;
    }



}