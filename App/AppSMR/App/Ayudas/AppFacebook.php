<?php

class AppFacebook{

	private $fb = false;
	private $AppId = '196778150840270';
	private $AppSecret = '04897715bb256fb523d2521d9b436261';

	/**
	 * AppFacebook constructor.
	 *
	 * Constructor que carga los archivos requeridos para utilizar el sdk de facebook.
	 */
	function __construct($AppId = false, $AppSecret = false) {
		self::requireFacebookSDK();
		if($AppId == true AND $AppId != '' AND $AppSecret == true AND $AppSecret != ''){
			$this->fb = new Facebook\Facebook([
				'app_id' => $AppId,
				'app_secret' => $AppSecret,
				'default_graph_version' => 'v2.7',
			]);
		}else{
			$this->fb = new Facebook\Facebook([
				'app_id' => $this->AppId,
				'app_secret' => $this->AppSecret,
				'default_graph_version' => 'v2.7',
			]);
		}
	}

	/**
	 * Metodo Privado
	 * requireFacebookSDK()
	 *
	 * Cargador de archivos del SDK V5.
	 * @throws AppExcepcion
	 */
	private function requireFacebookSDK(){
		$Archivo = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'facebook-sdk-v5', 'autoload.php'));
		$Validacion = array_search($Archivo, get_included_files());
		if($Validacion == false AND is_numeric($Validacion) == false):
			if(file_exists($Archivo) == true):
				require_once $Archivo;
			else:
				throw new AppExcepcion('No se encontró el SDK de Facebook');
			endif;
		endif;
		unset($Archivo, $Validacion);
	}

	/**
	 * Metodo Publico Estatico
	 * ObtenerUrlLogin()
	 *
	 * Devuelve el url para iniciar sesion en facebook
	 * @return string
	 */
	public function ObtenerUrlLogin(){
		$helper = $this->fb->getRedirectLoginHelper();
		$permissions = ['email'];
		$callback = NeuralRutasApp::RutaUrlApp('LoginFacebook', 'FacebookCallback');
		$loginUrl = $helper->getLoginUrl($callback, $permissions);
		return $loginUrl;
	}

	/**
	 * Metodo Publico
	 * EstablecerDefaultAccessToken($AccessToken = false)
	 *
	 * Establece el token por defecto en las llamadas a la  API Graph
	 * IMPORTANTE: Si no se establece el token de acceso por defecto, las llamadas a la API Graph no devolveran ningun resultado
	 * @param bool $AccessToken
	 */
	public function EstablecerDefaultAccessToken($AccessToken = false){
		if($AccessToken == true AND $AccessToken != ''){
			$this->fb->setDefaultAccessToken($AccessToken);
		}
	}

	/**
	 * Metodo Publico
	 * ObtenerAccessToken()
	 *
	 * Devuelve el token de acceso generado al iniciar sesion con facebook
	 * @return array|string
	 */
	public function ObtenerAccessToken(){
		$helper = $this->fb->getRedirectLoginHelper();
		try {
			$accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			return array(
				'Status'=>'Error',
				'Type'=>'Graph',
				'Message'=>$e->getMessage(),
			);
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			return array(
				'Status'=>'Error',
				'Type'=>'SDK',
				'Message'=>$e->getMessage(),
			);
		}

		if (isset($accessToken)) {
			$_SESSION['facebook_access_token'] = (string) $accessToken;
			return (string) $accessToken;
		}else{
			return array(
				'Status'=>'Error',
				'Type'=>'Token',
				'Message'=>'No se encontró un token de acceso',
			);
		}
	}

	/**
	 * Metodo Publico
	 * ObtenerInformacionUsuario()
	 *
	 * Realiza una solicitud GET a la API Graph para obtener informacion del usuario que inicia sesion
	 * @return array
	 */
	public function ObtenerInformacionUsuario(){
		try {
			$request = $this->fb->sendRequest(
				'GET',
				'/me',
				array(
					'fields' => 'id,name,email'
				)
			);
			$graphObject = $request->getGraphNode();
			return $graphObject->asArray();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		}
	}

	/**
	 * Metodo Publico
	 * ExtenderAccessToken($AccessToken = false)
	 *
	 * Extiende la vida del token de acceso a 60 dias
	 * @param bool $AccessToken
	 * @return \Facebook\Authentication\AccessToken
	 */
	public function ExtenderAccessToken($AccessToken = false){
		if($AccessToken == true AND $AccessToken != ''){
			$oAuth2Client = $this->fb->getOAuth2Client();
			$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($AccessToken);
			return $longLivedAccessToken;
		}
	}



}