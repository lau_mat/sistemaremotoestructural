<?php
	
	class AppUtilidades{

		/**
		 * Metodo publico estatico
		 * ObtenerColumnaCoincidencia($Consulta, $Matriz, $CampoCondicion)
		 *
		 * Obtiene el campo Seleccionado en los registros del primer arreglo, con valor segun la condicion.
		 * Seleccionado: Si se cumple que el campo recibido como condicion existen y tienen el mismo valor
		 * en el arreglo de Consulta y el arreglo Matriz
		 * "": Si los valores de los campos correspondientes no son los mismos en los arreglos Consulta y Matriz.
		 * @param $Consulta : Arreglo de registros incremental, al que se le agregan el campo Seleccionado a sus registros.
		 * @param $Matriz : Arreglo de registros incremental, con el que se comparan los valores del arreglo Consulta.
		 * @param $CampoCondicion : Campo donde deben coincidir.
		 * @return bool
		 */
		public static function ObtenerColumnaCoincidencia($Consulta, $Matriz, $CampoCondicion){
			if (isset($Consulta) == true AND is_array($Consulta) == true AND isset($Matriz) == true AND is_array($Matriz) == true AND isset($CampoCondicion) == true) {
				$Seleccionado = false;
				$Resultado = [];
				foreach ($Consulta as $KeyConsulta=>$ArrayConsulta) {
					foreach ($Matriz as $KeyMatriz=>$ArrayMatriz) {
						if (array_key_exists($CampoCondicion, $ArrayConsulta) == true AND array_key_exists($CampoCondicion, $ArrayMatriz) == true AND $ArrayConsulta[$CampoCondicion] == $ArrayMatriz[$CampoCondicion]) {
							$Seleccionado = true;
							$Resultado[] = array_merge($ArrayConsulta, array('Seleccionado' => 'Seleccionado'));
						}
					}
					if ($Seleccionado == false) {
						$Resultado[] = array_merge($ArrayConsulta, array('Seleccionado' => ''));
					}
					$Seleccionado = false;
				}
				return $Resultado;
			}
		}

	}