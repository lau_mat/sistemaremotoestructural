#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); // RX, TX
String s = "";
char c = 0;
int wait = 0, times = 0;
boolean newline = false;


void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
}

void loop() {
  if (wait > 0) {
    SerialBluetooth();
  } else {
    ciclo();
  }
  delay(1000);
}
void SerialBluetooth() {
  while (mySerial.available() > 0) {
    if (wait == 1) {
      mySerial.println(s);
      s = "";
      wait = 2;
    }
    c = mySerial.read();
    if (c == '\r') {
      continue;
    } else if (c == '\n') {
      newline = true;
      break;
    } else {
      s = s + c;
    }
  }
  if (newline) {
    Serial.println(s);
    s = "";
    newline = false;
  }
  delay(times);
}

void ciclo() {
  while (Serial.available() > 0) {
    c = Serial.read();
    if (c == '\r') {
      continue;
    } else if (c == '\n') {
      newline = true;
      break;
    } else {
      s = s + c;
    }
  }
  if (newline) {
    String val = "";
    for (int j = 4; j < s.length(); j++) {
      val = val + s[j];
    }
    times = val.toInt();
    newline = false;
    wait = 1;
  }
  delay(500);
}

