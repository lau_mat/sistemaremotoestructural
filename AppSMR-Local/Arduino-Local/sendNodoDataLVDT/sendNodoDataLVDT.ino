/*para configurar lectura*/
String s = "";
char c = 0;
int wait = 0;
boolean newline = false;
int times = 0;
int valorInicio = 0, valorInicio2 = 0, valorInicio3 = 0, valorInicio4 = 0;
/*Sensores*/
String muestra = "";

void setup() {
  Serial.begin(9600);
}
void loop() {
  if (wait > 0) {
    if (s[0] == 'a') {
      ReadChannel1();
    }
    if (s[1] == 'b') {
      ReadChannel2();
    }
    if (s[2] == 'c') {
      ReadChannel3();
    }
    if (s[3] == 'd') {
      ReadChannel4();
    }
    Serial.println(muestra);
    muestra = "";
    delay(times);
  } else {
    ciclo();
    delay(1000);
  }

}
void ciclo() {
  while (Serial.available() > 0) {
    c = Serial.read();
    if (c == '\r') {
      continue;
    } else if (c == '\n') {
      newline = true;
      break;
    } else {
      s = s + c;
    }
  }
  if (newline) {
    String val = "";
    for (int j = 4; j < s.length(); j++) {
      val = val + s[j];
    }
    Serial.print("dato: ");
    Serial.println(val);
    times = val.toInt();
    newline = false;
    wait = 1;
  }
  delay(500);
}
int cont = 0;
void ReadChannel1() {
  cont++;
  if (cont == 1) {
    valorInicio = balanceador(analogRead(A4), 0, 1023, 0, 10);
  }
  float result = balanceador(analogRead(A4), 0, 1023, 0, 10);
  result = result - valorInicio;
  muestra.concat(result);
  muestra.concat(" ");
}
int cont2 = 0;
void ReadChannel2() {
  cont2++;
  if (cont2 == 1) {
    valorInicio2 = balanceador(analogRead(A5), 0, 1023, 0, 10);
  }
  float result = balanceador(analogRead(A5), 0, 1023, 0, 10);
  result = result - valorInicio2;
  muestra.concat(result);
  muestra.concat(" ");
}
int cont3 = 0;
void ReadChannel3() {
 
  float result = balanceador(analogRead(A6), 0, 1023, 0, 10);
  muestra.concat(analogRead(A6));
  muestra.concat(" ");
}
int cont4 = 0;
void ReadChannel4() {
  cont4++;
  if (cont4 == 1) {
    valorInicio4 = balanceador(analogRead(A7), 0, 1023, 0, 10);
  }
  float result = balanceador(analogRead(A7), 0, 1023, 0, 10);
  result = result - valorInicio4;
  muestra.concat(result);
}

float balanceador(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

