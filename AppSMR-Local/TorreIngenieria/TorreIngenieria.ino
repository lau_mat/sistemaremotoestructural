/*para configurar lectura*/
String s = "";
char c = 0;
int wait = 0;
boolean newline = false;
int times = 0;
/*Sensores*/
int bit_array[25], bit_array2[25], bit_array3[25], bit_array4[25];
unsigned long time_now, time_now2, time_now3, time_now4;
String muestra = "";
int CLOCK_PIN = 4;
int DATA_PIN = 5; //PWM

int CLOCK_PIN2 = 7;
int DATA_PIN2 = 6; //PWM

int CLOCK_PIN3 = 8;
int DATA_PIN3 = 9; //PWM

int CLOCK_PIN4 = 12;
int DATA_PIN4 = 11; //PWM

void setup() {
  Serial.begin(9600);
  //Canal Uno
  pinMode(CLOCK_PIN, INPUT);
  pinMode(DATA_PIN, INPUT);
  //Canal Dos
  pinMode(CLOCK_PIN2, INPUT);
  pinMode(DATA_PIN2, INPUT);
  //Canal Tres
  pinMode(CLOCK_PIN3, INPUT);
  pinMode(DATA_PIN3, INPUT);
  //Canal Cuatro
  pinMode(CLOCK_PIN4, INPUT);
  pinMode(DATA_PIN4, INPUT);
}
void loop() {
  ReadChannel1();
  //ReadChannel2();

  Serial.println(muestra);
  muestra = "";
  delay(1000);
}
void ReadChannel1() {
  while (digitalRead(CLOCK_PIN) == LOW) {}
  time_now =  micros();
  while (digitalRead(CLOCK_PIN) == HIGH) {}
  if ((micros() - time_now) > 500) {
    decodeCUno();
  }
}
void ReadChannel2() {
  while (digitalRead(CLOCK_PIN2) == LOW) {}
  time_now2 =  micros();
  while (digitalRead(CLOCK_PIN2) == HIGH) {}
  if ((micros() - time_now2) > 500) {
    decodeCDos();
  }
}
void ReadChannel3() {
  while (digitalRead(CLOCK_PIN3) == LOW) {}
  time_now3 =  micros();
  while (digitalRead(CLOCK_PIN3) == HIGH) {}
  if ((micros() - time_now3) > 500) {
    decodeCTres();
  }
}
void ReadChannel4() {
  while (digitalRead(CLOCK_PIN4) == LOW) {}
  time_now4 =  micros();
  while (digitalRead(CLOCK_PIN4) == HIGH) {}
  if ((micros() - time_now4) > 500) {
    decodeCCuatro();
  }
}

//Canal Uno
void decodeCUno() {
  int sign = 1;
  int i = 0;
  float value = 0.0;
  float result = 0.0;
  bit_array[i] = digitalRead(DATA_PIN); // Store the 1st bit (start bit) which is always 1.
  while (digitalRead(CLOCK_PIN) == HIGH) {};

  for (i = 1; i <= 24; i++) {
    while (digitalRead(CLOCK_PIN) == LOW) { } // Wait until clock returns to HIGH
    bit_array[i] = digitalRead(DATA_PIN);
    while (digitalRead(CLOCK_PIN) == HIGH) {} // Wait until clock returns to LOW
  }

  for (i = 1; i <= 20; i++) { // Turning the value in the bit array from binary to decimal.
    value = value + (pow(2, i - 1) * bit_array[i]);
  }
  if (bit_array[21] == 1) sign = -1; // Bit 21 is the sign bit. 0 -> +, 1 => -

  result = (value * sign) / 100.00;
  muestra.concat(result); // Resultado de la impresión con 2 decimales.
  muestra.concat(" ");
  delay(10);
}

//Canal Dos
void decodeCDos() {
  int sign = 1;
  int i = 0;
  float value = 0.0;
  float result = 0.0;
  bit_array2[i] = digitalRead(DATA_PIN2); // Store the 1st bit (start bit) which is always 1.
  while (digitalRead(CLOCK_PIN2) == HIGH) {};

  for (i = 1; i <= 24; i++) {
    while (digitalRead(CLOCK_PIN2) == LOW) { } // Wait until clock returns to HIGH
    bit_array2[i] = digitalRead(DATA_PIN2);
    while (digitalRead(CLOCK_PIN2) == HIGH) {} // Wait until clock returns to LOW
  }
  for (i = 1; i <= 20; i++) { // Turning the value in the bit array from binary to decimal.
    value = value + (pow(2, i - 1) * bit_array2[i]);
  }
  if (bit_array2[21] == 1) sign = -1; // Bit 21 is the sign bit. 0 -> +, 1 => -

  result = (value * sign) / 100.00;
  muestra.concat(result); // Resultado de la impresión con 2 decimales.
  muestra.concat(" ");
  delay(10);
}

//Canal Tres
void decodeCTres() {
  int sign = 1;
  int i = 0;
  float value = 0.0;
  float result = 0.0;
  bit_array3[i] = digitalRead(DATA_PIN3); // Store the 1st bit (start bit) which is always 1.
  while (digitalRead(CLOCK_PIN3) == HIGH) {};

  for (i = 1; i <= 24; i++) {
    while (digitalRead(CLOCK_PIN3) == LOW) { } // Wait until clock returns to HIGH
    bit_array3[i] = digitalRead(DATA_PIN3);
    while (digitalRead(CLOCK_PIN3) == HIGH) {} // Wait until clock returns to LOW
  }
  for (i = 1; i <= 20; i++) { // Turning the value in the bit array from binary to decimal.
    value = value + (pow(2, i - 1) * bit_array3[i]);
  }
  if (bit_array3[21] == 1) sign = -1; // Bit 21 is the sign bit. 0 -> +, 1 => -

  result = (value * sign) / 100.00;
  muestra.concat(result); // Resultado de la impresión con 2 decimales.
  muestra.concat(" ");
  delay(10);
}
//Canal Cuatro
void decodeCCuatro() {
  int sign = 1;
  int i = 0;
  float value = 0.0;
  float result = 0.0;
  bit_array3[i] = digitalRead(DATA_PIN4); // Store the 1st bit (start bit) which is always 1.
  while (digitalRead(CLOCK_PIN4) == HIGH) {};

  for (i = 1; i <= 24; i++) {
    while (digitalRead(CLOCK_PIN4) == LOW) { } // Wait until clock returns to HIGH
    bit_array4[i] = digitalRead(DATA_PIN4);
    while (digitalRead(CLOCK_PIN4) == HIGH) {} // Wait until clock returns to LOW
  }
  for (i = 1; i <= 20; i++) { // Turning the value in the bit array from binary to decimal.
    value = value + (pow(2, i - 1) * bit_array4[i]);
  }
  if (bit_array4[21] == 1) sign = -1; // Bit 21 is the sign bit. 0 -> +, 1 => -

  result = (value * sign) / 100.00;
  muestra.concat(result); // Resultado de la impresión con 2 decimales.
  delay(10);
}
