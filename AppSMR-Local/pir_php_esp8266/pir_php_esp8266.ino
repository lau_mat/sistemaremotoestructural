#include "ESP8266WiFi.h"
#include "ESP8266WiFiMulti.h"
#include "ESP8266HTTPClient.h"
ESP8266WiFiMulti WiFiMulti;
HTTPClient http;
const char* red = "ExperimentoDistancia";//ssid de la red
const char* pass = "ExpDist!23";//contraseña
String direccion = "http://www.sensoriuscloud.com/sensoriusapp/SensoriusApp/DataProcessing?";//direccion

byte mac [6];

void setup () {
  Serial.begin(9600);
  pinMode(2, INPUT);
}

void loop() {
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  Serial.println("antes de concetarse a la red");
  WiFiMulti.addAP(red, pass);
  String dirMac = "";
  WiFi.macAddress(mac);
  String temp;
  for (int i = 0; i < 6; i++) {
    temp = String(mac[i], HEX);
    if (temp.length() < 2) {
      dirMac += String("0") + temp;
    } else {
      dirMac += temp;
    }
    if (i < 5) {
      dirMac += ':';
    }
  }
  dirMac.toUpperCase();
  while (WiFiMulti.run() != WL_CONNECTED) {
    delay(500);
  }
  Serial.println("Conectado a la red");
  Serial.println(WiFi.localIP());


  String estado = "", sendGet = "";
  while (WiFiMulti.run() == WL_CONNECTED) {
    int valor = digitalRead(2);

    if (valor == HIGH) {
      estado = "ACTIVIDAD";
      Serial.println("Lectura enviada");
      Serial.println(dirMac + "," + "ACTIVIDAD");
      delay(1000);
    } else {
      estado = "SIN_ACTIVIDAD";
      Serial.println("Lectura enviada");
      Serial.println(dirMac + "," + "SIN_ACTIVIDAD");
      delay(1000);
    }
    sendGet = direccion+"Mac=" + dirMac + "&Estado=" + estado;
    http.begin(sendGet);
    http.GET();
    http.end();
    delay(600000);
  }

}
