
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Instrumentation;

/**
 *
 * @author Carlos Laureano
 */
public class Instrumentation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Development created by Roberto Carlos Laureano Mata");
        System.out.println("Master's Degree Student of the MIIDT, Faculty of Engineering - University Autonomous of Guerrero, Mexico");
        System.out.println("========================================================");
        Configuration config = new Configuration();
        config.setVisible(true);
    }
}
