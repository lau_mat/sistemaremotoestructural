-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: estructuraremota_db
-- ------------------------------------------------------
-- Server version	5.6.29-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_activida`
--

DROP TABLE IF EXISTS `tbl_activida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_activida` (
  `IdActivida` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCanalActivo` bigint(20) NOT NULL,
  `Registro` varchar(20) DEFAULT NULL,
  `FechaHora` datetime DEFAULT NULL,
  PRIMARY KEY (`IdActivida`),
  KEY `fk_tbl_activida_tbl_canales_activos1_idx` (`IdCanalActivo`),
  CONSTRAINT `fk_tbl_activida_tbl_canales_activos1` FOREIGN KEY (`IdCanalActivo`) REFERENCES `tbl_canales_activos` (`IdCanalActivo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_activida`
--

LOCK TABLES `tbl_activida` WRITE;
/*!40000 ALTER TABLE `tbl_activida` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_activida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_canales`
--

DROP TABLE IF EXISTS `tbl_canales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_canales` (
  `IdCanal` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IdCanal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_canales`
--

LOCK TABLES `tbl_canales` WRITE;
/*!40000 ALTER TABLE `tbl_canales` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_canales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_canales_activos`
--

DROP TABLE IF EXISTS `tbl_canales_activos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_canales_activos` (
  `IdCanalActivo` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdDetalleInstrumentacion` bigint(20) NOT NULL,
  `IdCanal` bigint(20) NOT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdCanalActivo`),
  KEY `fk_tbl_canalesactivos_tbl_canales1_idx` (`IdCanal`),
  KEY `fk_tbl_canales_activos_tbl_detalle_instrumentacion1_idx` (`IdDetalleInstrumentacion`),
  CONSTRAINT `fk_tbl_canales_activos_tbl_detalle_instrumentacion1` FOREIGN KEY (`IdDetalleInstrumentacion`) REFERENCES `tbl_detalle_instrumentacion` (`IdDetalleInstrumentacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_canalesactivos_tbl_canales1` FOREIGN KEY (`IdCanal`) REFERENCES `tbl_canales` (`IdCanal`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_canales_activos`
--

LOCK TABLES `tbl_canales_activos` WRITE;
/*!40000 ALTER TABLE `tbl_canales_activos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_canales_activos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_detalle_instrumentacion`
--

DROP TABLE IF EXISTS `tbl_detalle_instrumentacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_detalle_instrumentacion` (
  `IdDetalleInstrumentacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdInstrumentacion` bigint(20) NOT NULL,
  `IdSensor` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDetalleInstrumentacion`),
  KEY `fk_tbl_detalle_instrumentacion_tbl_sensores1_idx` (`IdSensor`),
  KEY `fk_tbl_detalle_instrumentacion_tbl_instrumentacion1_idx` (`IdInstrumentacion`),
  CONSTRAINT `fk_tbl_detalle_instrumentacion_tbl_instrumentacion1` FOREIGN KEY (`IdInstrumentacion`) REFERENCES `tbl_instrumentacion` (`IdInstrumentacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_detalle_instrumentacion_tbl_sensores1` FOREIGN KEY (`IdSensor`) REFERENCES `tbl_sensores` (`IdSensor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_detalle_instrumentacion`
--

LOCK TABLES `tbl_detalle_instrumentacion` WRITE;
/*!40000 ALTER TABLE `tbl_detalle_instrumentacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_detalle_instrumentacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_encargados`
--

DROP TABLE IF EXISTS `tbl_encargados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_encargados` (
  `IdInformacion` bigint(20) NOT NULL,
  `IdInstrumentacion` bigint(20) NOT NULL,
  `Principal` varchar(15) NOT NULL,
  KEY `fk_tbl_encargados_tbl_informacion_usuarios1_idx` (`IdInformacion`),
  KEY `fk_tbl_encargados_tbl_instrumentacion1_idx` (`IdInstrumentacion`),
  CONSTRAINT `fk_tbl_encargados_tbl_informacion_usuarios1` FOREIGN KEY (`IdInformacion`) REFERENCES `tbl_informacion_usuarios` (`IdInformacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_encargados_tbl_instrumentacion1` FOREIGN KEY (`IdInstrumentacion`) REFERENCES `tbl_instrumentacion` (`IdInstrumentacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_encargados`
--

LOCK TABLES `tbl_encargados` WRITE;
/*!40000 ALTER TABLE `tbl_encargados` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_encargados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_informacion_usuarios`
--

DROP TABLE IF EXISTS `tbl_informacion_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_informacion_usuarios` (
  `IdInformacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUsuario` bigint(20) NOT NULL,
  `Nombres` varchar(50) DEFAULT NULL,
  `ApellidoPaterno` varchar(50) DEFAULT NULL,
  `ApellidoMaterno` varchar(50) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Correo` varchar(100) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdInformacion`),
  KEY `fk_tbl_informacion_usuarios_tbl_sistema_usuarios1_idx` (`IdUsuario`),
  CONSTRAINT `fk_tbl_informacion_usuarios_tbl_sistema_usuarios1` FOREIGN KEY (`IdUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_informacion_usuarios`
--

LOCK TABLES `tbl_informacion_usuarios` WRITE;
/*!40000 ALTER TABLE `tbl_informacion_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_informacion_usuarios` VALUES (1,1,'Roberto','Laureano','Mata','7471001559','rclaureano@uagro.mx','ACTIVO'),(2,2,'Carlos','Laureano','Mata','7471001559','carlos.12.1992@gmail.com','ACTIVO');
/*!40000 ALTER TABLE `tbl_informacion_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_instrumentacion`
--

DROP TABLE IF EXISTS `tbl_instrumentacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_instrumentacion` (
  `IdInstrumentacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `Direccion` mediumtext,
  `Descripcion` mediumtext,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdInstrumentacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_instrumentacion`
--

LOCK TABLES `tbl_instrumentacion` WRITE;
/*!40000 ALTER TABLE `tbl_instrumentacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_instrumentacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sensores`
--

DROP TABLE IF EXISTS `tbl_sensores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sensores` (
  `IdSensor` bigint(20) NOT NULL AUTO_INCREMENT,
  `Mac` varchar(20) DEFAULT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdSensor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sensores`
--

LOCK TABLES `tbl_sensores` WRITE;
/*!40000 ALTER TABLE `tbl_sensores` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sensores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sistema_usuarios`
--

DROP TABLE IF EXISTS `tbl_sistema_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sistema_usuarios` (
  `IdUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdPerfil` bigint(20) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdUsuario`),
  KEY `fk_tbl_sistema_usuarios_tbl_sistema_usuarios_perfil_idx` (`IdPerfil`),
  CONSTRAINT `fk_tbl_sistema_usuarios_tbl_sistema_usuarios_perfil` FOREIGN KEY (`IdPerfil`) REFERENCES `tbl_sistema_usuarios_perfil` (`IdPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sistema_usuarios`
--

LOCK TABLES `tbl_sistema_usuarios` WRITE;
/*!40000 ALTER TABLE `tbl_sistema_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_sistema_usuarios` VALUES (1,1,'Administrador','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','ACTIVO'),(2,2,'Supervisor','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','ACTIVO');
/*!40000 ALTER TABLE `tbl_sistema_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sistema_usuarios_perfil`
--

DROP TABLE IF EXISTS `tbl_sistema_usuarios_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sistema_usuarios_perfil` (
  `IdPerfil` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `Control` varchar(15) DEFAULT NULL,
  `Error` varchar(15) DEFAULT NULL,
  `Administrador` varchar(15) DEFAULT NULL,
  `Supervisor` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IdPerfil`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sistema_usuarios_perfil`
--

LOCK TABLES `tbl_sistema_usuarios_perfil` WRITE;
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` DISABLE KEYS */;
INSERT INTO `tbl_sistema_usuarios_perfil` VALUES (1,'Administrador','ACTIVO','true','true','true','true'),(2,'Supervisor','ACTIVO','true','true','false','true');
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-25 13:46:28
